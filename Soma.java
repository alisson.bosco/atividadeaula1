public class Soma {

    public static int somarNumerosPares( int numero ) {
        int soma = 0;
        for ( int i = 0; i <= numero; i++ ) {
            soma += i;
        }
        return soma;
    }

    public static int somarNumerosImpares( int numero ) {
        int soma = 0;
        for ( int i = 0; i <= numero; i++ ) {
            if ( i % 2 != 0 ) {
                soma += i;
            }
        }
        return soma;
    }

    public static void main( String[] args ) {
        int numeroImpar = 3;
        int resultadoImpar = somarNumerosImpares( numeroImpar );
        System.out.println( "Soma de números ímpares de 0 até " + numeroImpar + ": " + resultadoImpar );
        int numeroPar = 2;
        int resultadoPar = somarNumerosPares( numeroPar );
        System.out.println( "Soma de números pares de 0 até " + numeroPar + ": " + resultadoPar );
    }
}
