import java.util.Arrays;

public class Desafio {

    public static void main( String[] args ) {
        double[][] notas = { { 7.5, 8.0, 9.5 }, { 6.0, 7.5, 8.5 }, { 9.0, 9.5, 9.0 } };

        Arrays.stream( notas ).mapToDouble( Desafio::calcularMedia )
                .forEach( media -> System.out.println( "A Média dos alunos foi: " + media ) );
    }

    public static double calcularMedia( double[] notas ) {
        return Arrays.stream( notas ).average().orElse( 0.0 );
    }
}
