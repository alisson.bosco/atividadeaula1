public class MultiploDeDez {

    public static void substituirMultiplosDez( int[] array ) {
        int multiploDez = 0;

        for ( int i = 0; i < array.length; i++ ) {
            if ( array[ i ] % 10 == 0 ) {
                multiploDez = array[ i ];
            } else if ( multiploDez != 0 ) {
                array[ i ] = multiploDez;
            }
        }
    }

    public static void main( String[] args ) {
        int[] array = { 1, 10, 11, 20, 12 };

        substituirMultiplosDez( array );

        System.out.print( "Array modificado: " );
        for ( int num : array ) {
            System.out.print( num + " " );
        }
    }
}