public class Extrato {
    private double janeiro;
    private double fevereiro;
    private double marco;

    public Extrato(double janeiro, double fevereiro, double marco) {
        this.janeiro = janeiro;
        this.fevereiro = fevereiro;
        this.marco = marco;
    }

    public void calcularDespesaTotal() {
        double despesaTotal = janeiro + fevereiro + marco;
        System.out.println("Despesa total no trimestre foi de: R$" + despesaTotal);
    }

    public void calcularMediaMensal() {
        double mediaMensal = (janeiro + fevereiro + marco) / 3;
        System.out.println("Média mensal de gastos foi de: R$" + mediaMensal);
    }

    public static void main(String[] args) {
        Extrato balanco = new Extrato(15000, 23000, 17000);
        balanco.calcularDespesaTotal();
        balanco.calcularMediaMensal();
    }
}
