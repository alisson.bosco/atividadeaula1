public class RemocaoDeCaracteres {

    public static String removeVogais( String texto ) {

        String vogais = "aeiouAEIOU";

        StringBuilder textoSemVogais = new StringBuilder();
        for ( int i = 0; i < texto.length(); i++ ) {
            char caractere = texto.charAt( i );
            if ( vogais.indexOf( caractere ) == -1 ) {
                textoSemVogais.append( caractere );
            }
        }

        return textoSemVogais.toString();
    }

    public static void main( String[] args ) {
        String texto = "Segue aqui um exemplo de texto com vogais";
        String textoSemVogais = removeVogais( texto );
        System.out.println( "Texto sem vogais: " + textoSemVogais );
    }
}
