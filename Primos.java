public class Primos {

    public static boolean verificarNumeroPrimo( int numero ) {
        if ( numero <= 1 ) {
            return false;
        }

        for ( int i = 2; i <= Math.sqrt( numero ); i++ ) {
            if ( numero % i == 0 ) {
                return false;
            }
        }

        return true;
    }

    public static void main( String[] args ) {
        int numero = 175;
        boolean ehPrimo = verificarNumeroPrimo( numero );

        if ( ehPrimo ) {
            System.out.println( numero + " é primo." );
        } else {
            System.out.println( numero + " não é primo." );
        }
    }
}
