public class ApenasUmEQuatro {

    public static boolean contemApenasUmEQuatro( int[] array ) {
        for ( int num : array ) {
            if ( num != 1 && num != 4 ) {
                return false;
            }
        }
        return true;
    }

    public static void main( String[] args ) {
        int[] array1 = { 1, 4, 1, 4 };
        int[] array2 = { 5, 9, 4, 1 };

        if ( contemApenasUmEQuatro( array1 ) ) {
            System.out.println( "O array1 Informado contém apenas os números 1 e 4." );
        } else {
            System.out.println( "O array1 Informado não contém apenas os números 1 e 4." );
        }
        if ( contemApenasUmEQuatro( array2 ) ) {
            System.out.println( "O array2 Informado contém apenas os números 1 e 4." );
        } else {
            System.out.println( "O array2 Informado não contém apenas os números 1 e 4." );
        }
    }
}
